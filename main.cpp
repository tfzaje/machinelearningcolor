#include <iostream>
#include "src/Simulation.h"

int main() {
    Simulation simulation;
    simulation.run();

    return 0;
}