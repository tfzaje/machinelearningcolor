//
// Created by Edwin on 29.06.2019.
//

#ifndef MASHINELEARNINGEXAMPLE_ELEMENT_H
#define MASHINELEARNINGEXAMPLE_ELEMENT_H

#include <src/EdlibHeader.hpp>
#include <SFML/Graphics.hpp>
#include <vector>

class Element : public ed::GenAElement{
private:
    sf::Color sfColor;
    ed::HsvColor hsvColor;
    ed::RgbColor inputColor;
    sf::CircleShape shape;

public:
    Element();

    GenAElement *clone() override;

    void setInput(sf::Color color);

    sf::Color getColor();
    sf::CircleShape* getDrawable();
};


#endif //MASHINELEARNINGEXAMPLE_ELEMENT_H
