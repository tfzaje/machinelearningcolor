//
// Created by Edwin on 29.06.2019.
//

#include "Simulation.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Element.h"
#include "Strategy.h"

void Simulation::run() {
    //window
    int windowWidth = 1600;
    int windowHeight = 900;

    sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "Machine Learning Is Cool");
    window.setFramerateLimit(60);

    sf::Font font;
    font.loadFromFile("../resources/fonts/arial.ttf");

    sf::Text text;
    text.setFont(font);
    text.setFillColor(sf::Color(0, 0, 0));
    text.setCharacterSize(14);
    text.setOrigin(10, 5);

    sf::Text endText;
    endText.setFont(font);
    endText.setFillColor(sf::Color(0, 128, 0));
    endText.setCharacterSize(22);
    endText.setPosition(windowWidth - 400, windowHeight - 200);
    endText.setString("");

    bool finished = false;
    bool done = false;
    bool stop = true;

    //target color setup
    sf::CircleShape targetColor;
    targetColor.setRadius(50.0);
    targetColor.setOrigin(50, 50);
    targetColor.setPosition(windowWidth / 2, windowHeight - 70);
    int r = 0;
    int g = 0;
    int b = 0;
    int mode = 0;

    std::cout << "Target-Color(r g b): " << std::endl;
    std::cin >> r >> g >> b;
    targetColor.setFillColor(sf::Color(r, g, b));

    std::cout << "Mode? (0 auto, 1 manuel)" << std::endl;
    std::cin >> mode;

    //algorithm setup
    Strategy strategy;
    strategy.setup(targetColor.getFillColor());
    Element* example = new Element();
    ed::GeneticAlgorithm geneticAlgorithm(&strategy, 100, 2, 100, true, 20, example);

    geneticAlgorithm.run();

    int trainingCounter = 0;
    int posOfBest = 0;

    //simulation loop
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
                if (event.type == sf::Event::KeyPressed) {
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
                        if (mode != 0) {
                            geneticAlgorithm.run();
                            trainingCounter++;
                        } else {
                            stop = !stop;
                        }
                    }
                }
        }

        if (finished && !done) {
            Element *currentElementToDraw = (static_cast<Element *> (geneticAlgorithm.getPopulation()->at(posOfBest)));
            ed::RgbColor endRGB(currentElementToDraw->getColor().r, currentElementToDraw->getColor().g, currentElementToDraw->getColor().b);
            ed::HsvColor endHSV(endRGB.getHSV().at(0), endRGB.getHSV().at(1), endRGB.getHSV().at(2));
            std::string s = "Finished!\nRGB: \n";
            s += std::to_string(endRGB.getR()) + ", " + std::to_string(endRGB.getG()) + ", " + std::to_string(endRGB.getB()) + "\n";
            s += "HSV: \n";
            s += std::to_string(endHSV.getHue()) + ", " +
                 std::to_string(endHSV.getSaturation() * 100) + ", " +
                 std::to_string(endHSV.getValue() * 100);
            s += "\nReached After " + std::to_string(trainingCounter) + " Generations";
            endText.setString(s);

            done = true;
        }

        if (mode == 0 && !finished && !stop) {
            geneticAlgorithm.run();
            trainingCounter++;
        }

        window.clear();
        window.draw(targetColor);
        for (int i = 0; i < geneticAlgorithm.getPopulation()->size(); i++) {
            Element* currentElementToDraw = (static_cast<Element*> (geneticAlgorithm.getPopulation()->at(i)));
            if (currentElementToDraw->getFitness() == 100) {
                finished = true;
                posOfBest = i;
            }
            window.draw(*currentElementToDraw->getDrawable());
            text.setPosition(currentElementToDraw->getDrawable()->getPosition());
            text.setString( std::to_string(ed::NumberHelper::roundNumber(currentElementToDraw->getFitness())));
            window.draw(text);
        }
        window.draw(endText);
        if (finished) {
            drawBrain(geneticAlgorithm.getPopulation()->at(posOfBest)->getBrain(), &window, &font, ed::IVector2(0, windowHeight - 300));
        }

        window.display();
    }
}
void Simulation::drawBrain(ed::NeuralNetwork *network, sf::RenderWindow *window, sf::Font *font, ed::IVector2 upperLeftCorner) {
    int inputFlagsX = upperLeftCorner.getX() + 50;
    int inputColumnX = inputFlagsX + 150;
    int hiddenColumnX = inputColumnX + 150;
    int outPutColumnX = hiddenColumnX + 150;
    int outputFlagsX = outPutColumnX + 50;

    std::vector<sf::Text> textsI;
    std::vector<sf::Text> textsH;
    std::vector<sf::Text> textsO;

    for (int i = 0; i < network->getInputNeurons()->size(); i++) {
        textsI.push_back(sf::Text());
        textsI.at(i).setFont(*font);
        textsI.at(i).setCharacterSize(14);
        textsI.at(i).setFillColor(sf::Color(0, 150, 255));
        std::string s = "";
        s += network->getInputNeurons()->at(i)->getName() + ": " + std::to_string(network->getInputNeurons()->at(i)->getValue());
        textsI.at(i).setString(s);
        textsI.at(i).setPosition(inputFlagsX, i * 60 + upperLeftCorner.getY() - 5);
    }

    for (int i = 0; i < network->getHiddenNeurons()->size(); i++) {
        textsH.push_back(sf::Text());
        textsH.at(i).setFont(*font);
        textsH.at(i).setCharacterSize(14);
        textsH.at(i).setFillColor(sf::Color(0, 150, 255));
        std::string s = "";
        s += std::to_string(network->getHiddenNeurons()->at(i)->getValue());
        textsH.at(i).setString(s);
        textsH.at(i).setPosition(hiddenColumnX - 20, i * 60 + upperLeftCorner.getY() - 18);
    }

    for (int i = 0; i < network->getOutputNeurons()->size(); i++) {
        textsO.push_back(sf::Text());
        textsO.at(i).setFont(*font);
        textsO.at(i).setCharacterSize(14);
        textsO.at(i).setFillColor(sf::Color(0, 150, 255));
        std::string s = "";
        s += network->getOutputNeurons()->at(i)->getName() + ": " + std::to_string(network->getOutputNeurons()->at(i)->getValue());
        textsO.at(i).setString(s);
        textsO.at(i).setPosition(outputFlagsX, i * 60 + upperLeftCorner.getY() - 5);
    }

    //neurons

    std::vector<sf::CircleShape*> inputNeurons;
    std::vector<sf::CircleShape*> hiddenNeurons;
    std::vector<sf::CircleShape*> outputNeurons;

    for (int i = 0; i < network->getInputNeurons()->size(); i++) {
        sf::CircleShape* shape = new sf::CircleShape();
        shape->setRadius(25.0);
        shape->setOutlineThickness(2);
        shape->setFillColor(sf::Color(50, 50, 50));
        shape->setOutlineColor(sf::Color(75, 75, 75));
        shape->setOrigin(25, 25);
        shape->setPosition(inputColumnX, i * 60 + upperLeftCorner.getY());
        inputNeurons.push_back(shape);
    }

    for (int i = 0; i < network->getHiddenNeurons()->size(); i++) {
        sf::CircleShape* shape = new sf::CircleShape();
        shape->setRadius(25.0);
        shape->setOutlineThickness(2);
        shape->setFillColor(sf::Color(50, 50, 50));
        shape->setOutlineColor(sf::Color(75, 75, 75));
        shape->setOrigin(25, 25);
        shape->setPosition(hiddenColumnX, i * 60 + upperLeftCorner.getY());
        hiddenNeurons.push_back(shape);
    }

    for (int i = 0; i < network->getOutputNeurons()->size(); i++) {
        sf::CircleShape* shape = new sf::CircleShape();
        shape->setRadius(25.0);
        shape->setOutlineThickness(2);
        shape->setFillColor(sf::Color(50, 50, 50));
        shape->setOutlineColor(sf::Color(75, 75, 75));
        shape->setOrigin(25, 25);
        shape->setPosition(outPutColumnX, i * 60 + upperLeftCorner.getY());
        outputNeurons.push_back(shape);
    }

    //connections
    sf::VertexArray inputToHidden(sf::Lines, inputNeurons.size() * hiddenNeurons.size() * 2);
    int inputToHiddenCounter = 0;
    sf::Color color;

    for (int i = 0; i < inputNeurons.size(); i++) {
        for (int j = 0; j < hiddenNeurons.size(); j++) {
            color = sf::Color(0, 0, 200);

            ed::Connection* tempConnection = network->getConnection(network->getHiddenNeurons()->at(j), network->getInputNeurons()->at(i));

            if (tempConnection->getValue() > 0) {
                color = sf::Color(0, tempConnection->getValue() * 200, 0);
            } else if (tempConnection->getValue() < 0) {
                color = sf::Color((-tempConnection->getValue()) * 200, 0, 0);
            }

            inputToHidden[inputToHiddenCounter].position = sf::Vector2f(hiddenNeurons.at(j)->getPosition().x, hiddenNeurons.at(j)->getPosition().y);
            inputToHidden[inputToHiddenCounter].color = color;

            inputToHidden[inputToHiddenCounter + 1].position = sf::Vector2f(inputNeurons.at(i)->getPosition().x, inputNeurons.at(i)->getPosition().y);
            inputToHidden[inputToHiddenCounter + 1].color = color;

            inputToHiddenCounter += 2;

            color = sf::Color(0, 0, 200);
        }
    }

    sf::VertexArray hiddenToOutput(sf::Lines, hiddenNeurons.size() * outputNeurons.size() * 2);
    int hiddenToOutputCounter = 0;

    for (int i = 0; i < hiddenNeurons.size(); i++) {
        for (int j = 0; j < outputNeurons.size(); j++) {
            color = sf::Color(0, 0, 200);

            ed::Connection* tempConnection = network->getConnection(network->getOutputNeurons()->at(j), network->getHiddenNeurons()->at(i));

            if (tempConnection->getValue() > 0) {
                color = sf::Color(0, tempConnection->getValue() * 200, 0);
            } else if (tempConnection->getValue() < 0) {
                color = sf::Color(tempConnection->getValue() * 200, 0, 0);
            }

            hiddenToOutput[hiddenToOutputCounter].position = sf::Vector2f(outputNeurons.at(j)->getPosition().x, outputNeurons.at(j)->getPosition().y);
            hiddenToOutput[hiddenToOutputCounter].color = color;

            hiddenToOutput[hiddenToOutputCounter + 1].position = sf::Vector2f(hiddenNeurons.at(i)->getPosition().x, hiddenNeurons.at(i)->getPosition().y);
            hiddenToOutput[hiddenToOutputCounter + 1].color = color;

            hiddenToOutputCounter += 2;

            color = sf::Color(0, 0, 200);
        }
    }

    //draw
    for (int i = 0; i < inputNeurons.size(); i++) {
        window->draw(*inputNeurons.at(i));
    }
    for (int i = 0; i < hiddenNeurons.size(); i++) {
        window->draw(*hiddenNeurons.at(i));
    }
    for (int i = 0; i < outputNeurons.size(); i++) {
        window->draw(*outputNeurons.at(i));
    }
    window->draw(inputToHidden);
    window->draw(hiddenToOutput);
    for (int i = 0; i < textsI.size(); i++) {
        window->draw(textsI.at(i));
    }
    for (int i = 0; i < textsH.size(); i++) {
        window->draw(textsH.at(i));
    }
    for (int i = 0; i < textsO.size(); i++) {
        window->draw(textsO.at(i));
    }
}
