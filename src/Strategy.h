//
// Created by Edwin on 29.06.2019.
//

#ifndef MASHINELEARNINGEXAMPLE_STRATEGY_H
#define MASHINELEARNINGEXAMPLE_STRATEGY_H

#include <src/EdlibHeader.hpp>
#include <SFML/Graphics.hpp>

class Strategy : public ed::GenAStrategy{
private:
    sf::Color targetColor;
    std::vector<ed::GenAElement*> tempList;

    void sortAndPosition(std::vector<ed::GenAElement *> *population);
public:
    void runFitnessTest(std::vector<ed::GenAElement *> *population) override;

    void setup(sf::Color target);

};


#endif //MASHINELEARNINGEXAMPLE_STRATEGY_H
