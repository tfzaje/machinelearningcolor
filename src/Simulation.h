//
// Created by Edwin on 29.06.2019.
//

#ifndef MASHINELEARNINGEXAMPLE_SIMULATION_H
#define MASHINELEARNINGEXAMPLE_SIMULATION_H


#include <src/EdlibHeader.hpp>
#include <SFML/Graphics.hpp>

class Simulation {
public:
    void run();

    void drawBrain(ed::NeuralNetwork *network, sf::RenderWindow *window, sf::Font *font, ed::IVector2 upperLeftCorner);
};


#endif //MASHINELEARNINGEXAMPLE_SIMULATION_H
