//
// Created by Edwin on 29.06.2019.
//

#include "Strategy.h"
#include "Element.h"

void Strategy::runFitnessTest(std::vector<ed::GenAElement *> *population) {
    int tempR = 0;
    int tempG = 0;
    int tempB = 0;

    for (int i = 0; i < population->size(); i++) {
        Element* current = static_cast<Element *>(population->at(i));

        current->setInput(targetColor);

        tempR = (int) ed::NumberHelper::absoluteValue(current->getColor().r - targetColor.r);
        tempG = (int) ed::NumberHelper::absoluteValue(current->getColor().g - targetColor.g);
        tempB = (int) ed::NumberHelper::absoluteValue(current->getColor().b - targetColor.b);

        current->setFitness(100 - (tempR + tempG + tempB));
    }

    sortAndPosition(population);
}
void Strategy::setup(sf::Color target) {
    targetColor = target;
}
void Strategy::sortAndPosition(std::vector<ed::GenAElement *> *population) {
    std::vector<ed::GenAElement*> toEmpty;
    int pos = 0;
    ed::GenAElement* current = nullptr;

    for (int i = 0; i < population->size(); i++) {
        toEmpty.push_back(population->at(i));
    }

    //sort
    while (!toEmpty.empty()) {
        for (int i = 0; i < toEmpty.size(); i++) {
            if (current == nullptr) {
                current = toEmpty.at(i);
                pos = i;
            } else {
                if (current->getFitness() < toEmpty.at(i)->getFitness()) {
                    current = toEmpty.at(i);
                    pos = i;
                }
            }
        }
        tempList.push_back(current);
        toEmpty.erase(toEmpty.begin() + pos);
        current = nullptr;
        pos = 0;
    }

    //set positions
    int yPos = 0;
    Element* currentElement = nullptr;

    for (int i = 0; i < tempList.size(); i++) {
        currentElement = static_cast<Element*> (tempList.at(i));

        if (i % 25 == 0) {
            yPos += 55;
        }

        currentElement->getDrawable()->setPosition(((i % 25) + 1) * 60, yPos);
    }

    //clear tempList
    tempList.clear();
}
