//
// Created by Edwin on 29.06.2019.
//

#include "Element.h"

ed::GenAElement *Element::clone() {
    ed::GenAElement* result = new Element();

    for (int i = 0; i < this->brain->getAllConnections().size(); i++) {
        result->getBrain()->getAllConnections().at(i)->setValue(this->getBrain()->getAllConnections().at(i)->getValue());
    }

    return result;
}
Element::Element() {
    shape = sf::CircleShape(25.0);
    sfColor = sf::Color(255, 255, 255);
    hsvColor = ed::HsvColor(0, 0.0, 1.0);
    inputColor = ed::RgbColor(255, 255, 255);

    shape.setOrigin(25, 25);
    shape.setFillColor(sfColor);

    std::vector<ed::InputNeuron*> inputs;
    std::vector<ed::OutputNeuron*> outputs;

    inputs.push_back(new ed::InputNeuron("red"));
    inputs.push_back(new ed::InputNeuron("green"));
    inputs.push_back(new ed::InputNeuron("blue"));

    outputs.push_back(new ed::OutputNeuron(3,"hue"));
    outputs.push_back(new ed::OutputNeuron(3,"saturation"));
    outputs.push_back(new ed::OutputNeuron(3,"value"));

    brain = new ed::NeuralNetwork(inputs, 5, outputs);
}
void Element::setInput(sf::Color color) {
    inputColor = ed::RgbColor(color.r, color.g, color.b);

    brain->getInputNeurons()->at(0)->setValue(inputColor.getR());
    brain->getInputNeurons()->at(0)->setDivisor(255.0);
    brain->getInputNeurons()->at(1)->setValue(inputColor.getG());
    brain->getInputNeurons()->at(1)->setDivisor(255.0);
    brain->getInputNeurons()->at(2)->setValue(inputColor.getB());
    brain->getInputNeurons()->at(2)->setDivisor(255.0);

    int newHue = (int) ((brain->getOutputNeurons()->at(0)->getValue() * 180.0) + 180.0);
    double newSaturation = (brain->getOutputNeurons()->at(1)->getValue() / 2.0) + 0.5;
    double newValue = (brain->getOutputNeurons()->at(2)->getValue() / 2.0) + 0.5;

    hsvColor.setHue(newHue);
    hsvColor.setSaturation(newSaturation);
    hsvColor.setValue(newValue);

    std::vector<int> rgbForSF = hsvColor.getRGB();

    sfColor = sf::Color(rgbForSF.at(0), rgbForSF.at(1), rgbForSF.at(2));

    shape.setFillColor(sfColor);
}
sf::CircleShape *Element::getDrawable() {
    return &shape;
}
sf::Color Element::getColor() {
    return sfColor;
}
