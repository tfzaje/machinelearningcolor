This program uses the libraries SFML and EdLib

The most simple way to setup SFML for the project is to follow the Instuructions on this site: https://www.sfml-dev.org/tutorials/2.5/start-vc.php;
I recommend to use visual studio. It is simple to setup SFML within it. (The easiest way however is to use cLion)

EdLib:
- Project in Visual Studio
- Tell Visual Studio to make a static library: settings > config > general>: make .exe to .lib if not already done.
- choose Debug or Release and x64 or x86. Note that this needs to mirror the settings of the main Programm.
- It may be necessary to remove all main methods (will be fixed)
- build the library

Main Programm:
- Project in Visual Studio
- Setup SFML (link above)
- Settings > C++ > General: add directory of EdLib where src/EdLibHeader.hpp is located. Note to use directory where src is located. (e.g: C:/EdLib/EdLib/ not: C:/EdLib/EdLib/src)
- Settings > Linker > General: add directory where your self-build EdLib.lib (if you didn't change the name) is located.
- Settings > Linker > Input: add EdLib.lib (if you didn't change the name).
- You can start the Main Programm now.
- if numbers are not shown, you need to locate the "resources"-directory one directory above your .exe